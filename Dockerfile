FROM debian:stretch

RUN echo "deb-src http://ftp.us.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable-src.list

RUN apt-get update && apt-get -y upgrade

WORKDIR /build

RUN apt-get install -y build-essential python3 python d-shlibs default-jdk-headless chrpath libarmadillo-dev libcharls-dev libdap-dev libepsilon-dev libfreexl-dev libfyba-dev libgeos-dev libgeotiff-dev libhdf4-alt-dev libhdf5-dev libmodern-perl-perl default-libmysqlclient-dev libnetcdf-dev libogdi3.2-dev libopenjp2-7-dev libpoppler-private-dev libproj-dev libqhull-dev libspatialite-dev liburiparser-dev libxerces-c-dev libzstd-dev netcdf-bin unixodbc-dev libfyba-dev libgeos-dev libgeotiff-dev libhdf4-alt-dev dpkg-dev debhelper lsb-release swig doxygen ant python-dev python3-dev python-numpy python3-numpy
RUN apt-get source python-gdal && bash -c "cd gdal-2.4.0+dfsg && dpkg-buildpackage -d -b -uc"

